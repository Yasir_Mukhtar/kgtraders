import 'dart:convert';

class Welcome {
  bool? status;
  String? error;
  List<Data>? data;

  Welcome({this.status, this.error, this.data});

  Welcome.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    error = json['error'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['error'] = this.error;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String? surveyId;
  String? applicantName;
  String? region;
  String? type;
  String? applicantContact;
  String? bankIcon;
  String? isSlip;
  String? isHr;
  String? bankImg;

  Data(
      {this.surveyId,
        this.applicantName,
        this.region,
        this.type,
        this.applicantContact,
        this.bankIcon,
        this.isSlip,
        this.isHr,
        this.bankImg});

  Data.fromJson(Map<String, dynamic> json) {
    surveyId = json['survey_id'];
    applicantName = json['applicant_name'];
    region = json['region'];
    type = json['type'];
    applicantContact = json['applicant_contact'];
    bankIcon = json['bank_icon'];
    isSlip = json['is_slip'];
    isHr = json['is_hr'];
    bankImg = json['bank_img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['survey_id'] = this.surveyId;
    data['applicant_name'] = this.applicantName;
    data['region'] = this.region;
    data['type'] = this.type;
    data['applicant_contact'] = this.applicantContact;
    data['bank_icon'] = this.bankIcon;
    data['is_slip'] = this.isSlip;
    data['is_hr'] = this.isHr;
    data['bank_img'] = this.bankImg;
    return data;
  }
}

class Datum {
  Datum({
    this.surveyId,
    this.applicantName,
    this.region,
    this.type,
    this.applicantContact,
    this.bankIcon,
    this.bankImg,
    this.isHr,
    this.isSlip,
  });

  int? surveyId;
  String? applicantName;
  String? region;
  String? type;
  String? applicantContact;
  String? bankIcon;
  String? bankImg;
  int? isHr;
  int? isSlip;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        surveyId: json["survey_id"],
        applicantName: json['applicant_name'],
        region: json["region"],
        type: json["type"],
        applicantContact: json["applicant_contact"],
        bankIcon: json["bank_icon"],
        bankImg: json['bank_img'],
        isHr: json['is_hr'],
        isSlip: json['is_slip']
      );

  Map<String, dynamic> toJson() => {
        "survey_id": surveyId,
        'applicant_name': applicantName,
        "region": region,
        "type": type,
        "applicant_contact": applicantContact,
        "bank_icon": bankIcon,
        "bank_img": bankImg,
        "is_hr":isHr,
        "is_slip":isSlip
      };
}
