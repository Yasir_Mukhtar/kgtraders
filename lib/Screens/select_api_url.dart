import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:radium_tech/Components/form_button.dart';
import 'package:radium_tech/Global/global.dart';
import 'package:radium_tech/Screens/Auth/sign_in.dart';
import 'package:radium_tech/Utils/colors.dart';

class Select_Api_Url extends StatefulWidget {
  const Select_Api_Url({Key? key}) : super(key: key);

  @override
  _Select_Api_UrlState createState() => _Select_Api_UrlState();
}

class _Select_Api_UrlState extends State<Select_Api_Url> {
  var applicantJobStatus = [
    'https://testerp.radiumpk.com/',
    'http://192.168.18.65:8000/',
  ];
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Scaffold(
        body: Container(
          child: Container(
            width: 500,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(
                    width: size.width * 0.6,
                    image: const AssetImage(
                      "assets/kg-logo.png",
                    )),
                SizedBox(
                  height: 22,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Card(
                    elevation: 50,
                    shadowColor: appColor.withOpacity(0.5),
                    // color: c.withOpacity(.5),
                    color: Colors.white.withOpacity(.6),
                    child: Container(
                      width: 500,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: size.height * 0.03),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 40),
                            child: Text(
                              "Select Url",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: appColor,
                                  fontSize: 20),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          SizedBox(height: size.height * 0.03),
                          Container(
                            width: 250,
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                apiurl = value;
                                });
                              },
                                style: TextStyle(
                                  color: appColor,
                                ),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    labelText: 'Api Url',
                                    labelStyle: TextStyle(
                                      color:appColor,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: appColor),
                            ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: appColor),
                              ),
                                )

                              ),
                          ),
                          SizedBox(height: 10),
                          FormButton(
                              buttonTitle: "\t\tLogin\t\t",
                              onTap: () {
                                Navigator.pushReplacement(
                                    context, MaterialPageRoute(builder: (context) => LoginScreen()));
                              },
                              iconData: Icons.login),
                          SizedBox(height: 10),

                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
