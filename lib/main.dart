import 'package:flutter/material.dart';
import 'package:radium_tech/Screens/Auth/sign_in.dart';
import 'package:radium_tech/Utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Screens/TaskListing/user_list.dart';
import 'Screens/select_api_url.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var userName = prefs.getString('user');
  var userId = prefs.getString('userid');

  runApp(
    RootRestorationScope(
      restorationId: 'root',
      child: MaterialApp(
        theme: ThemeData(appBarTheme: AppBarTheme(

          elevation: 10,
          shadowColor: appColor,
          backgroundColor:appColor.withOpacity(.6) ,
          centerTitle: true,
        
          shape: RoundedRectangleBorder(
            side: BorderSide(color: appColor),

          )
        ),
        scaffoldBackgroundColor: Colors.grey.shade300
        ),
        debugShowCheckedModeBanner: false,
        home:  userName==null
            ? LoginScreen()
            : UserData(
                userId: userId!,
                userName: userName,
              ),
        //Select_Api_Url(),



      ),
    ),
  );
}